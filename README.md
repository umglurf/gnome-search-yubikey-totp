<!--
SPDX-FileCopyrightText: 2020 Håvard Moen <post@haavard.name>

SPDX-License-Identifier: GPL-3.0-or-later
-->

# gnome-search-yubikey-totp
Gnome search provider for copying yubikey totp codes to clipboard.

This will add a new search allowing you to search for totp codes stored on the
yubikey and copy to clipboard.

## Installation
You should install [Yubico Authenticator](https://developers.yubico.com/yubioath-desktop/).
This is usually available in the package manager.

To install the search provider, do
```
uvx --from build pyproject-build --installer uv
pip install dist/*.whl
```
